@extends('layouts.public')

@section('content')
    <div class="jumbotron">

        <div class="row">

            <div class="col-md-4">
                <h2>La rose du désert</h2>
                <p class="lead">Réservez vos vacances de rêves dans notre village !</p>
                <p><a class="btn btn-lg btn-success" href="/reserver" role="button">Réserver maintenant</a></p>
            </div>

            <div class="col-md-8">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="/images/image_accueil/1.jpg" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="/images/image_accueil/2.jpg" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="/images/image_accueil/3.jpg" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="/images/image_accueil/4.jpg" alt="First slide">
                        </div>
                        <div class="item">
                            <img src="/images/image_accueil/5.jpg" alt="First slide">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>
        <br><br><br>

        <p class="text-justify">

            La rose du désert a été inauguré en 2008, A la fois contemporaine et respectueuse de l'environnement, la rose du désert dispose d'une décoration dans laquelle tradition et modernité se marient avec harmonie.

            La rose du désert est dotée de toutes les commodités nécessaires, le complexe touristique dispose de diverses installations dédiées à l’hébergement et les loisirs, notamment un hôtel de 187 chambres, 72 villas, 51 "khaïmas" (tentes traditionnelles) équipées et de 14 appartements. Ce vaste complexe a été pensé pour accueillir des familles, des groupes de vacanciers des groupes professionnels et entreprises afin de mener à bien des congrès, des séminaires, team building et autres meetings.

            Notre village de vacance comporte aussi un terrain de golf de 100 hectares, réalisé selon les normes internationales, un sauna, des piscines, plusieurs restaurants, une grande salle de conférence et un musée, ainsi qu’une palmeraie de 150 hectares comptant plus de 20.000 palmiers et oliviers.

        </p>


    </div>

@endsection