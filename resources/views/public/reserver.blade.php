@extends('layouts.public')

@section('content')

    <h2>Réservation</h2>

    <form action="/reserver" method="post">
        <!-- CSRF token -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Séjour
                        </h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="arrival">Date d'arrivée</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input type="date" class="form-control" id="arrival" name="arrival" required
                                           autofocus/>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="departure">Date de départ</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input type="date" class="form-control" id="departure" name="departure" required/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="type">Type d'hébergement</label>
                            <select class="form-control" name="type" id="type">
                                @foreach($catalogues as $catalogue)
                                    <option value="{!! $catalogue->id !!}">{!! $catalogue->type !!}
                                        n° {!! $catalogue->numero !!} pour {!! $catalogue->capacite !!} personnes
                                        : {!! $catalogue->prix !!}€
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

            </div>


            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Paiement
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="cardNumber">
                                Numéro de carte de crédit</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="cardNumber" name="cardNumber"
                                       placeholder="################"
                                       required/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label for="expiryMonth">
                                        Date d'expiration</label>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input type="number" class="form-control" id="expiryMonth"
                                                   name="expiryMonth"
                                                   placeholder="MM" required min="1" max="12" step="1"/>
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="number" class="form-control" id="expiryYear" name="expiryYear"
                                                   placeholder="AA" required min="0" max="99" step="1"/>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label for="cvvCode">
                                        CVV</label>
                                    <input type="text" class="form-control" id="cvvCode" name="cvvCode"
                                           placeholder="CVV" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <div class="text-right">
            <button class="btn btn-primary btn-lg" type="submit">Réserver</button>
        </div>
    </form>


@endsection