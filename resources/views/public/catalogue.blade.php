@extends('layouts.public')

@section('content')


    <div class="col-md-2">
        <h2>Tente</h2>
    </div>
    <br><br><br>
    <div class="col-md-8">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/images/image_accueil/T1.jpg" alt="First slide">
                </div>
                <div class="item">
                    <img src="/images/image_accueil/T2.jpg" alt="First slide">
                </div>
                <div class="item">
                    <img src="/images/image_accueil/T3.jpg" alt="First slide">
                </div>
                <div class="item">
                    <img src="/images/image_accueil/T4.jpg" alt="First slide">
                </div>
                <div class="item">
                    <img src="/images/image_accueil/T5.jpg" alt="First slide">
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <br>

        <h4>
            - Un dépaysement garanti.<br>
            - Un confort optimal.<br>
            - Dormir à la belle étoile, une expérience unique.
        </h4>
        <br><br>

        <p class="Res"><a class=" text-center btn btn-lg btn-warning  " href="/reserver" role="button">Réserver maintenant</a></p>

    </div>



@endsection