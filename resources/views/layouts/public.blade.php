<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>La rose du désert</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/public.css">
    
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
</head>

<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/images/rose.png"/>  La rose du désert</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Notre village</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown01">
                        <li><a class="dropdown-item" href="">Pr&#233;sentation</a></li>
                        <li><a class="dropdown-item" href="">Historique</a></li>
                        <li><a class="dropdown-item" href="">Avis</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">H&#233;bergement</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown02">
                        <li><a class="dropdown-item" href="/catalogue-public">Nos tentes</a></li>
                        <li><a class="dropdown-item" href="/catalogue-public">Nos chambres</a></li>
                        <li><a class="dropdown-item" href="/catalogue-public">Nos suites</a></li>
                        <li><a class="dropdown-item" href="/catalogue-public">Nos appartements</a></li>
                        <li><a class="dropdown-item" href="/catalogue-public">Nos villas</a></li>

                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Restauration</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown03">
                        <li><a class="dropdown-item" href="">Le Mirage</a></li>
                        <li><a class="dropdown-item" href="">Le Vegan</a></li>
                        <li><a class="dropdown-item" href="">L'oriental</a></li>
                        <li><a class="dropdown-item" href="">Le Mechwi</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Nos Activités</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown04">
                        <li><a class="dropdown-item" href="">Hammam</a></li>
                        <li><a class="dropdown-item" href="">Circuit d&#233;couverte</a></li>
                        <li><a class="dropdown-item" href="">Soir&#233;es orientales</a></li>
                        <li><a class="dropdown-item" href="">Ski sur Sable</a></li>
                        <li><a class="dropdown-item" href="">Activit&#233;s sportives</a></li>
                        <li><a class="dropdown-item" href="">Cours de langue</a></li>
                        <li><a class="dropdown-item" href="">Ateliers</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Nos offres</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown05">
                        <li><a class="dropdown-item" href="">Weekend</a></li>
                        <li><a class="dropdown-item" href="">Vacances d'hiver</a></li>
                        <li><a class="dropdown-item" href="">Vacances de printemps</a></li>
                        <li><a class="dropdown-item" href="">Vacances d'&#233;t&#233;</a></li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/admin">Admin.</a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>


<main role="main" class="container">

    <div class="content-wrapper">
        @yield('content')
    </div>

</main><!-- /.container -->

<footer class="footer">
    <div class="container-fluid">

        <div class="row social">

            <div class="col-md-3 text-right">

                <a target="_blank" href="https://www.facebook.com/larosedudesert" class="facebook">
                    <i class="fab fa-facebook-square fa-2x"></i>
                </a>
                <a target="_blank" href="https://twitter.com/larosedudesert" class="twitter">
                    <i class="fab fa-twitter-square fa-2x"></i>
                </a>
                <a target="_blank" href="https://www.linkedin.com/larosedudesert" class="linkedin">
                    <i class="fab fa-linkedin fa-2x"></i>
                </a>
            </div>

            <div class="col-md-9 text-left">
                <div class="footer-contact">
                    <a href="mailto:service@larosedudesert.com">service@larosedudesert.com</a> |
                    <a href="tel:+213 21 24 85 96">+213 21 24 85 96</a> |
                    <a target="_blank" href="https://www.google.fr/maps/place/Sidi+Yahia,+Hydra,+Algeria">64 Chemin Sidi Yahia Hydra 16000 Alger, Algérie</a>
                </div>
            </div>

        </div>

    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>

<!--<script src="../../../../assets/js/vendor/popper.min.js"></script>   -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
