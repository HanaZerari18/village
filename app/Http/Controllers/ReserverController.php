<?php

namespace App\Http\Controllers;

use App\Models\Catalogue;
use App\Models\Reservations;
use App\Repositories\CatalogueRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReserverController extends Controller
{
    /** @var  CatalogueRepository */
    private $catalogueRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CatalogueRepository $catalogueRepo)
    {
        // accessible uniquement par les utilisateurs connectés
        $this->middleware('auth');

        $this->catalogueRepository = $catalogueRepo;
    }

    /**
     * Afficher le formulaire de réservation
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // obtenir le contenu du catalogue (select * from catalogue)
        $catalogues = $this->catalogueRepository->all();

        return view('public.reserver')->with('catalogues', $catalogues);
    }

    /**
     * Créer la réservation
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request)
    {
        $reservation = new Reservations();

        $reservation->arrivee = $request->arrival;
        $reservation->depart = $request->departure;
        $reservation->catalogue_id = $request->type;
        $reservation->user_id = Auth::user()->id;
        $reservation->prix = Catalogue::find($request->type)->prix;

        $reservation->save();

        return view('public.reservation-confirmation')->with('reservation', $reservation);
    }
}
