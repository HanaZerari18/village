<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * Controleur de base pour l'administration
 */
class AdminBaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //public function sendResponse($result, $message)
    //{
    //    return Response::json(ResponseUtil::makeResponse($message, $result));
    //}
    //
    //public function sendError($error, $code = 404)
    //{
    //    return Response::json(ResponseUtil::makeError($error), $code);
    //}
}
