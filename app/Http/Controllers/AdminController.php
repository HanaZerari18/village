<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends AdminBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin', []);

        return view('admin');
    }
}
