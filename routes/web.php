<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/catalogue-public', 'CataloguePublicController@index');

// GET /reserver
Route::get('reserver', 'ReserverController@index');

// POST /reserver
Route::post('reserver', 'ReserverController@post');

// Administration

Route::get('/admin', 'AdminController@index');

Route::get('/admin', 'AdminController@index')->name('admin');


Route::resource('catalogues', 'CatalogueAdminController');  // create, read, update, delete

Route::resource('clients', 'UserAdminController');  // create, read, update, delete

Route::resource('reservations', 'ReservationsAdminController');  // create, read, update, delete